    #include <Wire.h>
    #include "rgb_lcd.h"
    #include "MMA7660.h"
    #include <LGPS.h>
    
    gpsSentenceInfoStruct info;
    char buff[256];
    
    //PARTIE GPS
    static unsigned char getComma(unsigned char num,const char *str)
    {
      unsigned char i,j = 0;
      int len=strlen(str);
      for(i = 0;i < len;i ++)
      {
         if(str[i] == ',')
          j++;
         if(j == num)
          return i + 1; 
      }
      return 0; 
    }
    
    static double getDoubleNumber(const char *s)
    {
      char buf[10];
      unsigned char i;
      double rev;
      
      i=getComma(1, s);
      i = i - 1;
      strncpy(buf, s, i);
      buf[i] = 0;
      rev=atof(buf);
      return rev; 
    }
    
    static double getIntNumber(const char *s)
    {
      char buf[10];
      unsigned char i;
      double rev;
      
      i=getComma(1, s);
      i = i - 1;
      strncpy(buf, s, i);
      buf[i] = 0;
      rev=atoi(buf);
      return rev; 
    }
    
    void parseGPGGA(const char* GPGGAstr)
    {
    
      double latitude;
      double longitude;
      int tmp, hour, minute, second, num ;
      if(GPGGAstr[0] == '$')
      {
        tmp = getComma(1, GPGGAstr);
        hour     = (GPGGAstr[tmp + 0] - '0') * 10 + (GPGGAstr[tmp + 1] - '0');
        minute   = (GPGGAstr[tmp + 2] - '0') * 10 + (GPGGAstr[tmp + 3] - '0');
        second    = (GPGGAstr[tmp + 4] - '0') * 10 + (GPGGAstr[tmp + 5] - '0');
        
        sprintf(buff, "UTC timer %2d-%2d-%2d", hour, minute, second);
        Serial.println(buff);
        
        tmp = getComma(2, GPGGAstr);
        latitude = getDoubleNumber(&GPGGAstr[tmp]);
        tmp = getComma(4, GPGGAstr);
        longitude = getDoubleNumber(&GPGGAstr[tmp]);
        sprintf(buff, "latitude = %10.4f, longitude = %10.4f", latitude, longitude);
        Serial.println(buff); 
        
        tmp = getComma(7, GPGGAstr);
        num = getIntNumber(&GPGGAstr[tmp]);    
        sprintf(buff, "satellites number = %d", num);
        Serial.println(buff); 
      }
      else
      {
        Serial.println("Not get data"); 
      }
    }
    /// FIN PARTIE GPS
    
    
    
    rgb_lcd lcd;
    MMA7660 accelemeter;
    
    const int but = 8;
    const int buz = 7;
    const int buz2 = 2;
    const int led1 = 3;
    const int led2 = 6; 
     
    int colorR = 255;
    int colorG = 0;
    int colorB = 0;
    
    //changement d'état du bouton
    boolean oldSwitchState = LOW;
    boolean newSwitchState = LOW;
    
    boolean LEDstatus = LOW;
    
    
    
    
    /////////////FONCTIONS///////////////////
    
    
    void monSysteme() {
          newSwitchState = digitalRead(but);
          float ax, ay, az;
          accelemeter.getAcceleration(&ax, &ay, &az);
          
          if ( ax > 0.5 || ay > 0.5 || az > 0.5 ) {
    
              digitalWrite(led1, HIGH);
              digitalWrite(buz, HIGH);
              digitalWrite(buz2, LOW);
              digitalWrite(led2, LOW); 
              delay(200);          
              digitalWrite(led1, LOW);
              digitalWrite(led2, HIGH);
              digitalWrite(buz, LOW);
              digitalWrite(buz2, HIGH);
              delay(200);
              lcd.clear();
              lcd.setCursor(0, 0);
              lcd.print("ALERTE");
              int colorR= 255;
              int colorG = 135;
              lcd.setRGB(colorR, colorG, colorB);
              delay(200); //200 de base 
              Serial.begin(115200);
              LGPS.powerOn();
              Serial.println("LGPS Power on, and waiting ..."); 
              delay(3000);
            }
            else {
              digitalWrite(led1, LOW);
              digitalWrite(led2, LOW);
              int colorR= 0;
              int colorG = 255;
              lcd.setRGB(colorR, colorG, colorB);
              lcd.print("SECURITY : ON");
              delay(1000);
            }
    
        
          if(newSwitchState != oldSwitchState) {
             if ( newSwitchState == HIGH )
           {
               if ( LEDstatus == LOW ) { 
                delay(200); 
                digitalWrite(led1, LOW); 
                digitalWrite(led2, HIGH); 
                LEDstatus = HIGH;
                lcd.clear();
                lcd.setCursor(0, 0);
                lcd.print("SECURITY : ON");
                int colorR= 0;
                int colorG = 255;
                lcd.setRGB(colorR, colorG, colorB); 
                }
               else {
                delay(200); 
                digitalWrite(led1, HIGH);
                digitalWrite(led2, LOW);   
                digitalWrite(buz2, HIGH);
                digitalWrite(buz, LOW);   
                LEDstatus = LOW; 
                lcd.clear();
                lcd.setCursor(0, 0);
                lcd.print("SECURITY : OFF");
                int colorR= 255;
                int colorG = 0;
                lcd.setRGB(colorR, colorG, colorB); 
                }
           }
           
           oldSwitchState = newSwitchState;
            }
    
            
    }
    
        
    
    
    
    //////////////////////////////////////////////////
    
    
    void setup() {
      accelemeter.init();
      
      lcd.begin(16, 2);
      lcd.clear();
      lcd.setRGB(colorR, colorG, colorB);
      lcd.print("SECURITY :OFF");
    
      pinMode(led1, OUTPUT);
      pinMode(led2, OUTPUT);
      digitalWrite(led1,HIGH); 
      digitalWrite(led2,LOW); 
      pinMode(but, INPUT);
    
    }
    
    void loop() {
    
    monSysteme();  
     Serial.println((char*)info.GPGGA); 
      parseGPGGA((const char*)info.GPGGA);
      delay(2000);
    }

