

Ce code permet de faire sonner une alarme et clignoter une led quand le seuil du potentiomètre est dépasser:

    #define ROTARY_ANGLE_SENSOR A0
    #define LED 4  //the Grove - LED is connected to PWM pin D3 of Arduino
    #define ADC_REF 5 //reference voltage of ADC is 5v.If the Vcc switch on the seeeduino
                        //board switches to 3V3, the ADC_REF should be 3.3
    #define GROVE_VCC 5 //VCC of the grove interface is normally 5v
    #define FULL_ANGLE 300 //full value of the rotary angle is 300 degrees
    #define BUZZER 7
    
    void setup()
    {
       // Serial.begin(9600);
        pinMode(ROTARY_ANGLE_SENSOR, INPUT);
        pinMode(LED,OUTPUT);  
        pinMode(BUZZER, OUTPUT);
        digitalWrite(BUZZER,LOW);
    }
    
    void loop()
    {   
        float voltage;
        int sensor_value = analogRead(ROTARY_ANGLE_SENSOR);
        voltage = (float)sensor_value*ADC_REF/1023;
        float deg = (voltage*FULL_ANGLE)/GROVE_VCC;
        //Serial.println("The angle between the mark and the starting position:");
       // Serial.println(deg);
        if (deg > 100){
          digitalWrite(BUZZER, HIGH);
          digitalWrite(LED, HIGH);
          delay(30);
        }
         else {
          digitalWrite(BUZZER, LOW);
          digitalWrite(LED, LOW);
          delay(30);
         }
          
        
    
        int brightness;
        brightness = map(deg, 0, FULL_ANGLE, 0, 255);
       // analogWrite(LED,brightness);
        delay(500);
    }

