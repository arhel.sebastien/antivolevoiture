# Antivol connecté 

# Sujet

L’objectif du projet est de créer un système qui permettra de protéger un véhicule contre le vol. Pour cela, nous allons utiliser une carte LinkitOne qui va nous permettre de brancher des capteurs pour détecter une infraction, d’envoyer des informations et des alertes en cas de détection d’intrusion. Il faudra :

-   Monitorer l’environnement pour détecter des changements (détection de mouvement, de vibrations, de sons, de changement de température…)

-   Envoyer des alertes en cas de suspicions de vols (lumière, son, SMS…) et envoyer/enregistrer sa position GPS

-   Une action permettra d’activer et désactiver l’envoi d’alerte
    
-   En mode normal, afficher les informations sur un écran LCD (Les faire défiler automatiquement ou manuellement)

-   Monitorer le niveau de batterie, affiché son niveau et lancer une alerte en cas de batterie faible

Contraintes

-   Chaque étudiant.e est libre d’implémenter ses solutions techniques

## Brainstorming 
![brainstorming](https://lh3.googleusercontent.com/tHOx2UOSEuFNSoLPvi1_85y0kYmYZJnaP9qOLgBXba2UtyMwBgVldOJCjmAGYQYkdvy7I85r9ePI34PEpQ6f5YWjOlhNrATfl5vuXqW1PJiaqVWBiAKuQe5dRnYGXoWBFCnaNnsZt6Y2iMNawleT9Uz8QnqKndCx_bAkm1Zcsf8WdPcCw7OvLvJH67jgTHlIimRkyvnYcMNjIY_q59A_6k-4l7oqhusmCLhoKZtVVqwfsBTZlupKx1rnP8IhCM09I4lveStPwK3hR8tTNwROv7A4YuzGDyL61o97n4CreimFe-YND-y76YhYU2XxZHaCpnZAd7yt96Om9tjF1JFmZcA3clyNG4YyU_vxoj1Hzyd_NUp9dLs6Hi7-jyySK3kRKHimeEgaRkM0HjxMmQmY6SAQxecbHgN_yEe92zEd0kdz_ZWPTXvthqDe_U1tCGCLPoxIidruiL0RLz9QscPd4tZnZr8SCzR3xAqiFMAizJ7aXWUOfvMq0hSgYIRYBIqKfavCTbZ0h5ZLiriMIGHPWbeV7rB2a9MCpiO8fMWf5tb3wqi0vKeNDB45d5tIjYBRVQ8PYlscE6Qv919IWJEhWSgum6--WmaIklxfJb63Bhd-S7F5xsveI56E3v0bHLcawhTXv2a-RX6rHiHCgI0gqK4NzHK1WA8XXAVguqS6usIVLgnYgj4iZXQ=w1278-h1090-no)

On va traiter chaque bloc primaire, c’est à dire qu’on va créer un programme testé pour acquérir et afficher l’état de la batterie. Un autre qui va Acquérir et envoyer la position GPS, sur quoi on va l’envoyer je sais pas encore.

Détecter les vibration et actionner une alarme.

![Alerter](https://lh3.googleusercontent.com/nF7wX43cTWWuu68IQ7P4ODo7rLSCDfk2K-Cmzfy2m9Tqdpdje_ucqx3jDlKn_LVQ5LodvkRNAxtLiRR-QQ-_jjgYv3ZKKtSJilu4FUMu7VyPIgHuKWCbnk9yQfFPiGRdc7s0kOGG7vCk7hj4yWNJB_VnzSsvvId6FrSnPv0w4taMIku6KwMqJh1rMP24PTL6opYxHtC7Az2BfSrNHiOn3ugsP4smvAZMoXjioqQFNOhiX7axf5_LpGhb98-eYUQg79v_4OP-5RkryDumu5EYI3k4ZvKaSEqNUSKCYnQCjpMWhKM0peOAxl15MPbWoh5_FygQTOGrsV7FVpMJ75hs3Keor2qGQia3PY26mjwN0k0M8LCnFKTMJjT67y1xRyszcZe9zu7Na4Kj6LUtK8Nrw4FmN4tRFgK2Oz0BW87Wx8r1bf7lx1XvWwlpVt63w3v5bGdYpmkThvbGlLNl9tj4KkHVnzJ61qOnQPllpN9TLdEjnbm87DeWpSPTuNK7EKxkB5DEAY64Gj_3C8_YG54eseT62quUo6Q3bK8GfbeSNYzZr0nA5K7OLNzIDQ5nqWl4j4_XhS1BIAmqtBd0PcXZt-_DwtyciQkaJ9J7x5PMO0jPqOh3mgPMct6L1RjK9BJ1bmRO9DsL-pxFXMHt3nVJq0WnNAb8GxBcVl5CojFiDWrkSJVZ4kGA_Ng=w550-h646-no)
