//LE GPS EST EN ATTENTE POUR LE MOMENT YA UN PETIT 
//PROBLEME LA   
  #include <Wire.h>
    #include "rgb_lcd.h"
    #include "MMA7660.h"
    #include <LGSM.h>
    
    rgb_lcd lcd;
    MMA7660 accelemeter;
    
    const int but = 8;
    const int led1 = 3;
    const int led2 = 6; 
     
    int colorR = 255;
    int colorG = 0;
    int colorB = 0;
    
    char phonenum[20] = "0692598202";
    
    //changement d'état du bouton
    boolean oldSwitchState = LOW;
    boolean newSwitchState = LOW;
    
    boolean LEDstatus = LOW;
    
    
    
    
    /////////////FONCTIONS///////////////////
    
    
    
    
    void monSysteme() {
          newSwitchState = digitalRead(but);
          float ax, ay, az;
          accelemeter.getAcceleration(&ax, &ay, &az);
          
          if ( ax > 0.5 || ay > 0.5 || az > 0.5) {
            
              digitalWrite(led1, HIGH);
              digitalWrite(led2, LOW);
              delay(200);
              digitalWrite(led1, LOW);
              digitalWrite(led2, HIGH);
              delay(200);
              lcd.clear();
              lcd.setCursor(0, 0);
              lcd.print("ALERTE");
              int colorR= 255;
              int colorG = 135;
              lcd.setRGB(colorR, colorG, colorB);
              LSMS.beginSMS(phonenum);
              LSMS.print("test");
              LSMS.endSMS();
              delay(200);
    
            }
            else {
              digitalWrite(led1, LOW);
              digitalWrite(led2, LOW);
              lcd.print("SECURITY : ON");
              int colorR= 0;
              int colorG = 255;
              lcd.setRGB(colorR, colorG, colorB); 
              delay(200);
            }
    
        
          if(newSwitchState != oldSwitchState) {
             if ( newSwitchState == HIGH )
           {
               if ( LEDstatus == LOW ) { 
                delay(200); 
                digitalWrite(led1, LOW); 
                digitalWrite(led2, HIGH); 
                LEDstatus = HIGH;
                lcd.clear();
                lcd.setCursor(0, 0);
                lcd.print("SECURITY : ON");
                int colorR= 0;
                int colorG = 255;
                lcd.setRGB(colorR, colorG, colorB); 
                }
               else {
                delay(200); 
                digitalWrite(led1, HIGH);
                digitalWrite(led2, LOW);   
                LEDstatus = LOW; 
                lcd.clear();
                lcd.setCursor(0, 0);
                lcd.print("SECURITY : OFF");
                int colorR= 255;
                int colorG = 0;
                lcd.setRGB(colorR, colorG, colorB); 
                }
           }
           
           oldSwitchState = newSwitchState;
            }
    
            
    }
    
        
    
    
    
    //////////////////////////////////////////////////
    
    
    void setup() {
      accelemeter.init();
      
      lcd.begin(16, 2);
      lcd.clear();
      lcd.setRGB(colorR, colorG, colorB);
      lcd.print("SECURITY :OFF");
    
      pinMode(led1, OUTPUT);
      pinMode(led2, OUTPUT);
      digitalWrite(led1,HIGH); 
      digitalWrite(led2,LOW); 
      pinMode(but, INPUT);
    
    }
    
    void loop() {
    
    monSysteme();  
    
    }

